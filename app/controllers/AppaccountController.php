<?php

class AppaccountController extends Action{
	
	public function showIndex(){
		$this->view()->display('file:appaccount/index.tpl');
	}
	
	public function showSearch(){
		$this->view()->display('file:appaccount/search.tpl');
	}
	
	public function showView(){
		$var = self::getAccountService()->getAll();
		$this->view()->assign('arr', $var);
		$this->view()->display('file:appaccount/view.tpl');
	}
	
	public function add(){
		self::getAccountService()->add($_POST['name'], 1);
		$this->view()->assign('success',"注册成功!");
		$this->view()->display('file:appaccount/index.tpl');
	}
	
	public function getOne(){
		$var = self::getAccountService()->getOne($_POST['id']);
		$this->view()->assign('arr', $var);
		$this->view()->display('file:appaccount/search.tpl');
	}
	
	public function getAll(){//这里实现导出功能
		$this->view()->display('file:appaccount/view.tpl');
	}
	
	public static function getAccountService(){
		return new AppAccountService();
	}
}