<?php
class OrderModel extends Db {
	protected $_order_list = 'w_order_list'; /*订单列表*/
	protected $_order_info = 'w_order_info'; /*订单信息*/
	protected $_order_conf = 'w_order_conf'; /*订单信息*/
	protected $_order_history = 'w_order_history'; /*操作历史记录*/
	

	/*获取等待审核的订单*/
	public function getVerifyOrder() {
		$sql = "select a.uid,a.id,a.address,a.remark,a.addtime,a.orderno,shr,goods_name,b.num,b.coupons,b.coupons_total from w_order_list a left join w_order_info b on a.orderno=b.orderno where  status=1 order by shr";
		return $this->fetchAll ( $sql );
	}
	
	/**
	 * 获取历史记录
	 * @param unknown_type $where
	 */
	public function getHistory($where) {
		return $this->getAll ( $this->_order_history, $where );
	}
	public function countOrderHisotry($where) {
		if (! empty ( $where )) {
			$where = $this->batchWhere ( $where );
			$sql = "SELECT count(id) num FROM $this->_order_history WHERE $where";
		} else {
			$sql = "SELECT count(id) num FROM $this->_order_history";
		}
		
		$rs = $this->fetch ( $sql );
		return $rs ['num'];
	}
	/**
	 * 获取产品价格
	 * Enter description here ...
	 * @param unknown_type $where
	 */
	public function getGoodsExtendByGid($where) {
		return $this->getAll ( 'w_extend_value', $where );
	}
	public function getGoodsByGid($where) {
		return $this->getOne ( 'w_news_list', $where );
	}
	/**
	 * 获取产品信息
	 * @param unknown_type $orderno
	 */
	public function getGoodsInfo($orderno) {
		$sql = "select a.*,b.thumb from w_order_info a LEFT JOIN w_news_list b ON a.goods_id=b.id WHERE a.orderno='$orderno'";
		return $this->fetchAll ( $sql );
	}
	/**
	 * 添加历史记录
	 * Enter description here ...
	 * @param unknown_type $params
	 */
	public function addHistory($orderno, $remark, $action) {
		$this->add ( $this->_order_history, array ('orderno' => $orderno, 'remark' => $remark, 'action' => $action, 'action_time' => time () ) );
	}
	/**
	 * 获取产品全部价格
	 * Enter description here ...
	 */
	public function getGoodsPrice() {
		$sql = "select a.id,a.cid,a.title,b.`decimal` from w_news_list a left join w_extend_value b on a.id=b.gid where b.key='dljg';";
		return $this->fetchAll ( $sql );
	}
	/**
	 * 获取sno*/
	public function getSno() {
		return $this->getOne ( $this->_order_conf, array ('name' => 'sno' ) );
	}
	/**
	 * 获取订单数量
	 * 
	 */
	public function getOrderNum() {
		return $this->getOne ( $this->_order_conf, array ('name' => 'num' ) );
	}
	/**
	 * 根据订单号获取产品信息
	 * Enter description here ...
	 * @param unknown_type $orderno
	 */
	public function getGoodsBySNO($orderno) {
		return $this->getAll ( $this->_order_info, array ('orderno' => $orderno ) );
	}
	/**
	 * 修改订单状态
	 * Enter description here ...
	 * @param unknown_type $v
	 * @param unknown_type $where
	 */
	public function saveStatus($v, $where) {
		$this->update ( $this->_order_list, $v, $where );
	}
	/**
	 * 修改订单中产品状态
	 * Enter description here ...
	 * @param unknown_type $v
	 * @param unknown_type $where
	 */
	public function saveGoodsStatus($v, $where) {
		$this->update ( $this->_order_info, $v, $where );
	}
	/**
	 * 订单列表
	 *
	 * @param array $where
	 * contentId
	 */
	public function orderPage($where = NULL, $start, $end, $type = NULL) {
		if (empty ( $where )) {
			$sql = "SELECT * FROM $this->_order_list ORDER BY  id DESC LIMIT $start,$end";
		} elseif ($type == NULL && ! empty ( $where )) {
			$where = $this->batchWhere ( $where );
			$sql = "SELECT * FROM $this->_order_list WHERE $where ORDER BY  id DESC LIMIT $start,$end";
		}
		if ($type == 'status') {
			$sql = "SELECT * FROM $this->_order_list WHERE `status` IN ($where) ORDER BY  id DESC LIMIT $start,$end";
		}
		return $this->fetchAll ( $sql );
	
	}
	/*单号*/
	public function getOrderByWhere($where) {
		return $this->getAll ( $this->_order_list, $where,null,'id DESC' );
	}
	/**
	 * 获取订单数量
	 * Enter description here ...
	 * @param unknown_type $key
	 * @param unknown_type $where
	 */
	public function getOrderTotalNum() {
		$sql = "SELECT count(id) num FROM $this->_order_list";
		return $this->fetch ( $sql );
	
	}
	/**
	 * 添加订单
	 *
	 * @param unknown_type $params
	 */
	public function addOrder($params) {
		$this->add ( $this->_order_list, $params );
		return $this->lastInsertId ();
	}
	/**
	 * 添加订单详情
	 * @param unknown_type $params
	 */
	public function addGoods($params) {
		return $this->add ( $this->_order_info, $params );
	}
	/**
	 * 删除产品
	 * Enter description here ...
	 * @param unknown_type $where
	 */
	public function removeGoods($where) {
		return $this->delete ( $this->_order_info, $where );
	}
	/**
	 * 获取订单中某个产品信息
	 * Enter description here ...
	 * @param unknown_type $where
	 */
	public function getOrderInfo($where) {
		return $this->getOne ( $this->_order_info, $where );
	}
	/**
	 * 统计订单格式
	 */
	public function countOrder($where) {
		$where = $this->batchWhere ( $where );
		$sql = "SELECT count(id) num FROM $this->_order_list WHERE $where";
		return $this->fetch ( $sql );
	}
	/**
	 * 返回OrderModel
	 * @return OrderModel
	 */
	public static function instance() {
		return parent::_instance ( __CLASS__ );
	}
}